Note: the web directory is not mounted by default. To do so, login into the container


docker exec -it cont_name /bin/bash

and:

rm -Rf /var/www/html

ln -s /home/USER/FOLDER /var/www/html

